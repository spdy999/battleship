import Vue from 'vue';
import App from './App.vue';
import Vuex from 'vuex';
import vuetify from './plugins/vuetify';

Vue.config.productionTip = false;
Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    countClick: 0,
    countMiss: 0,
    vers: [],
    hors: [],
    sinkVersShow: false,
    sinkHorsShow: false,
    sinkVers: [],
    sinkHors: [],
    allSink: [],
    hello: 'hello',
    dialog: false,
    dialogText: '',
    wrongDialog: false,
  },
  mutations: {
    setVers(state, vers) {
      state.vers = vers;
    },
    setHors(state, hors) {
      state.hors = hors;
    },
    setCountClick(state) {
      state.countClick++;
    },
    setCountMiss(state) {
      state.countMiss++;
    },
    setAllSink(state, ship) {
      state.allSink = [...state.allSink, ship];
      if (state.allSink.length == 4) {
        state.dialog = true;
        state.dialogText = 'Win';
      }
    },
    setWrongDialog(state, payload) {
      state.wrongDialog = payload;
    },
    setDialog(state, payload) {
      state.dialog = payload;
    },
    setDialogText(state, payload) {
      state.dialogText = payload;
    },
    setSinkVers(state, payload) {
      state.sinkVers = [...state.sinkVers, payload];
    },
    setSinkHors(state, payload) {
      state.sinkHors = [...state.sinkHors, payload];
    },
    setSinkHorsShow(state, payload) {
      state.sinkHorsShow = payload;
    },
    setSinkVersShow(state, payload) {
      state.sinkVersShow = payload;
    },
  },
});

new Vue({
  vuetify,
  store,
  render: h => h(App),
}).$mount('#app');
